package ch.shinungo.babyname.controller;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import ch.shinungo.babyname.CsvBabynamen.CsvBabynamen;
import ch.shinungo.babyname.model.Babynamen;
import ch.shinungo.babyname.model.Suchparameter;
import ch.shinungo.babyname.persistence.PersistenceBaby;
import ch.shinungo.babyname.view.AddingHandler;
import ch.shinungo.babyname.view.AnimationSpaceHandler;
import ch.shinungo.babyname.view.BottomLabel;
import ch.shinungo.babyname.view.GenderHandler;
import ch.shinungo.babyname.view.LetterProposal;
import ch.shinungo.babyname.view.MenuAdminHandler;
import ch.shinungo.babyname.view.MenuStandardHandler;
import ch.shinungo.babyname.view.RadioButtonHandler;
import ch.shinungo.babyname.view.RemovingHandler;
import ch.shinungo.babyname.view.SearchHandler;
import ch.shinungo.babyname.view.SpacerView;
import ch.shinungo.babyname.view.TabellenAnsicht;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

public class Controller extends Application {

	public static final String FILE_OPEN = "open";
	public static final String FILE_SAVE = "save";
	private ObservableList<Babynamen> babynamenList;
	private PersistenceBaby persistance;
	private MenuStandardHandler menuHandler;
	private MenuAdminHandler mtoenuHandler;
	private AnimationSpaceHandler animationHandler;
	private RadioButtonHandler radioButtonHandler;
	private GenderHandler genderHandler;
	private LetterProposal letterProposal;
	private AddingHandler addingHandler;
	private RemovingHandler removingHandler;
	private TabellenAnsicht tabellenAnsicht;
	private Stage stage;
	private BottomLabel bottomLabel;
	private SearchHandler searchHandler; 
	private String user = "ff";
	private String pw = "ff";
	private String checkUser, checkPw;
	private Scene scene;
	private SpacerView spacerView;
	private MenuBar menuView;
	private MenuBar menu2View;
	private VBox createGridForAddingNames;
	private HBox createButtonForDeletingNames;
	private VBox vBoxGender;
	private VBox vBabyTabelle; 
	private GridPane animationBox; 
	private VBox radioButtonBox; 
	private HBox letterProBox; 
	private HBox searchBox; 
	private Label bottomLabelview; 
	private int indexPositionAnimation = 0;
	private HBox placeHolder;
	private String currentFilePath = null;
	
	public Controller() {
		this.menuHandler = new MenuStandardHandler(this);
		this.mtoenuHandler = new MenuAdminHandler(this);
		this.animationHandler = new AnimationSpaceHandler();
		this.genderHandler = new GenderHandler();
		this.radioButtonHandler = new RadioButtonHandler();
		this.letterProposal = new LetterProposal();
		this.addingHandler = new AddingHandler(this);
		this.removingHandler = new RemovingHandler(this);
		this.persistance = new CsvBabynamen();
		this.tabellenAnsicht = new TabellenAnsicht(this);
		this.babynamenList = FXCollections.observableArrayList(persistance.getAll(getDefaultFile()));
		this.bottomLabel = new BottomLabel(); 
		this.searchHandler = new SearchHandler(this);
		this.spacerView = new SpacerView();

		menuView = menuHandler.constructMenu(); 
		menu2View = mtoenuHandler.constructMenu();
		vBoxGender = genderHandler.constructGenderBox();
		vBabyTabelle = tabellenAnsicht.showTabellenAnsicht();
		createGridForAddingNames = addingHandler.createGridForAddingNames();
		createButtonForDeletingNames = removingHandler.createButtonForDeletingNames();
		animationBox = animationHandler.constructAnimation();
		radioButtonBox = radioButtonHandler.constructRadioButtons();
		letterProBox = letterProposal.createLetterProposal(); 
		searchBox = searchHandler.createSearchHandler(); 
		bottomLabelview = bottomLabel.makeAbottomText(); 
		placeHolder = spacerView.makeSpacer();
	}

	public static void startingMachine(String[] args) {
		Application.launch(args);
	}

	public void start(Stage stage) throws Exception {
		this.stage = stage;
		showStandardView();
	}

	public void showStandardView() throws IOException {
		scene = new Scene(new Group());
		scene.getStylesheets().add("styly.css");
		VBox vbox = new VBox();
		vbox.setId("vboxCss");
		vbox.getChildren().add(menuView); 
		vbox.getChildren().add(animationBox); 
		vbox.getChildren().add(vBoxGender);
		vbox.getChildren().add(radioButtonBox);
		vbox.getChildren().add(letterProBox);
		vbox.getChildren().add(searchBox);
		vbox.getChildren().add(vBabyTabelle);
		vbox.getChildren().add(bottomLabelview);

		((Group) scene.getRoot()).getChildren().addAll(vbox);
		stage.setTitle("BabyNamen - Final");
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
	}

	public void showLogInView() throws IOException {
		ObservableList<Node> children = ((Group) scene.getRoot()).getChildren();
		VBox vbox = (VBox) children.get(0);
		tabellenAnsicht.openWritableFunction();
		vbox.getChildren().remove(vBoxGender);
		vbox.getChildren().remove(radioButtonBox);
		vbox.getChildren().remove(letterProBox);
		vbox.getChildren().remove(searchBox);
		vbox.getChildren().remove(bottomLabelview);
		vbox.getChildren().remove(menuView);

		vbox.getChildren().add(indexPositionAnimation, menu2View);
		vbox.getChildren().add(indexPositionAnimation + 2, placeHolder);
		vbox.getChildren().add(indexPositionAnimation + 4, createButtonForDeletingNames);
		vbox.getChildren().add(indexPositionAnimation + 5, createGridForAddingNames);
		vbox.getChildren().add(indexPositionAnimation + 6, bottomLabelview);
	}

	public void removeViewsByLogOUt() {
		ObservableList<Node> children = ((Group) scene.getRoot()).getChildren();
		tabellenAnsicht.closeWritableFunction();
		VBox vbox = (VBox) children.get(0);
		vbox.getChildren().remove(menu2View);
		vbox.getChildren().remove(createGridForAddingNames);
		vbox.getChildren().remove(createButtonForDeletingNames);
		vbox.getChildren().remove(placeHolder);
		vbox.getChildren().remove(bottomLabelview);

		vbox.getChildren().add(indexPositionAnimation, menuView);
		vbox.getChildren().add(indexPositionAnimation + 2, vBoxGender);
		vbox.getChildren().add(indexPositionAnimation + 3, radioButtonBox);
		vbox.getChildren().add(indexPositionAnimation + 4, letterProBox);
		vbox.getChildren().add(indexPositionAnimation + 5, searchBox);
		vbox.getChildren().add(indexPositionAnimation + 7, bottomLabelview);
	}

	public void exitDecision() {
		Optional<ButtonType> result = displayAlert(Alert.AlertType.CONFIRMATION, "Baby verlassen?");
		if (result.isPresent() && result.get() == ButtonType.OK) {
			System.exit(0);
		}
	}

	public Optional<ButtonType> displayAlert(Alert.AlertType type, String alertText) {
		Alert alert = new Alert(type);
		alert.setTitle("BabyNamen");
		alert.setHeaderText(alertText);
		return alert.showAndWait();
	}

	public void saveDecision() {
		File file = new File(getDefaultFile());
		try (FileWriter fw = new FileWriter(file)) {
			displayAlert(Alert.AlertType.CONFIRMATION, "gespeichert!!");
			fw.write(toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loginDecision() {

		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Login Dialog");
		ButtonType loginButtonH = new ButtonType("Login", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonH, ButtonType.CANCEL);

		GridPane loginGrid = new GridPane();
		loginGrid.setHgap(10);
		loginGrid.setVgap(10);
		loginGrid.setAlignment(Pos.CENTER);
		loginGrid.setPadding(new Insets(25, 25, 25, 25));

		TextField txtUserName = new TextField();
		txtUserName.setPromptText("Username");

		PasswordField passwordfield = new PasswordField();
		passwordfield.setPromptText("Password");
		Label lblMessage = new Label();

		loginGrid.add(new Label("Username:"), 0, 0);
		loginGrid.add(txtUserName, 1, 0);
		loginGrid.add(new Label("Password:"), 0, 1);
		loginGrid.add(passwordfield, 1, 1);
		loginGrid.add(lblMessage, 1, 2);

		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonH);
		loginButton.setDisable(true);
		txtUserName.textProperty().addListener((observable, oldValue, newValue) -> {
			loginButton.setDisable(newValue.trim().isEmpty());
		});
		dialog.getDialogPane().setContent(loginGrid);
		Platform.runLater(() -> txtUserName.requestFocus());

		dialog.setResultConverter(dialogButton1 -> {
			checkUser = txtUserName.getText().toString().trim();
			checkPw = passwordfield.getText().toString();
			if (dialogButton1 == loginButtonH && checkUser.equals(user) && checkPw.equals(pw)) {
				System.out.println("Zutritt");
				try {
					showLogInView();
					clearSearchParameter();
					passwordfield.clear();
					txtUserName.clear();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				System.out.println("KEIN ZUTRITT");
			}
			return null;
		});
		@SuppressWarnings("unused")
		Optional<Pair<String, String>> result = dialog.showAndWait();
	}

	private String getDefaultFile() {
		if (currentFilePath == null) {
			return PersistenceBaby.DEFAULT_CSV_FILE;
		} else {
			return currentFilePath;
		}
	}

	public void openFileChooser(String openOrSave) {
        FileChooser fileChooser = new FileChooser();     
        configureFileChooser(fileChooser);
		if (openOrSave.equals(FILE_OPEN)) {
			System.out.println("File wird geöffnet");
			File file = fileChooser.showOpenDialog(stage);
			if (file != null) {
				currentFilePath = file.getAbsolutePath();
				openFile(file);
			}
		} else {
			System.out.println("File wird gespeichert");
			File file = fileChooser.showSaveDialog(stage);
			if (file != null) {
				saveFile(file);
				getDefaultFile();
			}
		}
	}

	public void storeBabynamen(Babynamen baby) {
		babynamenList.add(baby);
		persistance.saveBabyname(baby, getDefaultFile());
	}

	public void removeSelectedBabynamen() {
		Babynamen selectedBaby = tabellenAnsicht.getSelectedBaby();
		persistance.removeBabyname(selectedBaby, getDefaultFile());
		babynamenList.remove(selectedBaby);
	}

	public ObservableList<Babynamen> getBabynamen() {
		return babynamenList;
	}
        
	private void openFile(File file) {
		System.out.println("File wird geladen");
		babynamenList.clear();
		List<Babynamen> all = persistance.getAll(file.getAbsolutePath());
		babynamenList.addAll(all);
	}

	private void saveFile(File file) {
		persistance.saveAll(babynamenList, file.getAbsolutePath());
	}

	private void configureFileChooser(FileChooser fileChooser) {
		fileChooser.setTitle("View Pictures");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
            }	
	
	/*
	 * In Methode overallSearch werden die Suchattributte bereitgestellt
	 */

	public void overallSearch() {

		String geschlecht = genderHandler.getGeschlecht();
		System.out.println(geschlecht);
	
		String letter = letterProposal.getLetter(); 
		System.out.println(letter);
		
		boolean top200CH = radioButtonHandler.getTop200Ch().isSelected(); 
		System.out.println("top 200CH: "+ top200CH);
			
		boolean top100CH = radioButtonHandler.getRbTop100Ch().isSelected(); 
		System.out.println("top 100CH: "+ top100CH);
		
		boolean top100ww = radioButtonHandler.getRbTop100Ww().isSelected(); 
		System.out.println("top 100ww: "+ top100ww);
		
		boolean topBibl = radioButtonHandler.getRbTopBibl().isSelected(); 
		System.out.println("top Bibl: "+ topBibl);

		/*
		 * Suchattributte werden in die Suchparameter übergeben.
		 */

		Suchparameter sp = new Suchparameter();
		sp.setGeschlecht(geschlecht);
		sp.setBeginntMit(letter);	
		sp.setTop200CH(top200CH);
		sp.setrbTop100Ch(top100CH);
		sp.setisrbTop100Ww(top100ww);
		sp.setisrbTopBibl(topBibl);
		
		/*
		 * Clearing, Such-Ausführung und Erstellung der Liste anhand der Parameter
		 */
		babynamenList.clear();
		List<Babynamen> search = persistance.search(sp, getDefaultFile());
		babynamenList.addAll(search);
	}
	
	public void clearDisplayedList() {
		babynamenList.clear();
		babynamenList.addAll(persistance.getAll(getDefaultFile()));
	}
	
	public void clearSearchParameter() {
		clearDisplayedList();
		genderHandler.clearButtons();
		letterProposal.clearLetterField();
		radioButtonHandler.deSelcetRadioButtons();
	}
}