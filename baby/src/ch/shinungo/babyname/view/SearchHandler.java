package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class SearchHandler {
	private Controller controller;

	public SearchHandler(Controller controller) {
	this.controller = controller;
	}
	Button buttonProposal = new Button("Suchen");
	Button buttonClear = new Button("Suche Löschen");
	
	public HBox createSearchHandler() {
		buttonProposal.setOnAction(clearByPushEventHandler());
		buttonClear.setOnAction(clearParameter());
		HBox hbox = new HBox(7, buttonProposal, buttonClear);
		hbox.setId("searchButtonsCSS");
		return hbox;
	}
	private EventHandler<ActionEvent> clearParameter() {
		EventHandler<ActionEvent> clearByPush = event -> {
			controller.clearSearchParameter();
		};
		return clearByPush;
	}
	private EventHandler<ActionEvent> clearByPushEventHandler() {
		EventHandler<ActionEvent> clearByPush = event -> {
			controller.overallSearch();
		};
		return clearByPush;
	}
}
