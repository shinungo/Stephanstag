package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class RadioButtonHandler {
	// private GridPane grid;
	private Label namechoostext;
	private RadioButton rbTop200Ch;
	private RadioButton rbTop100Ch;
	private RadioButton rbTop100Ww;
	private RadioButton rbTopBibl;

	public VBox constructRadioButtons() {
		namechoostext = new Label("Weitere Auswahlkriterien:");
		namechoostext.setId("namechoosetextCSS");
		rbTop200Ch = (new RadioButton("Top200 Schweiz im Jahr 2017"));
		rbTop100Ch = (new RadioButton("Top100 aller Namen in der Schweiz"));
		rbTop100Ww = (new RadioButton("Top100 aller Namen auf der Welt"));
		rbTopBibl = (new RadioButton("Biblischer Name"));
		rbTop200Ch.setId("rbTop200chCss");
		rbTop100Ch.setId("rbrbTop100ChCss");
		rbTop100Ww.setId("rbrbTop100WwCss");
		rbTopBibl.setId("rbrbTopBiblCss");
		GridPane grid = new GridPane();
		grid.add(getTop200Ch(), 0, 1);
		grid.add(getRbTop100Ch(), 0, 2);
		grid.add(getRbTop100Ww(), 0, 3);
		grid.add(getRbTopBibl(), 0, 4);
		VBox shellBox = new VBox(7, namechoostext, grid);
		return shellBox;
	}
	
	public void deSelcetRadioButtons() {
	getTop200Ch().setSelected(false);
	getRbTop100Ch().setSelected(false);
	getRbTop100Ww().setSelected(false);
	getRbTopBibl().setSelected(false);
	}

	public RadioButton getTop200Ch() {
		return rbTop200Ch;
	}
	public void setTop200Ch(RadioButton top200Ch) {
		this.rbTop200Ch = top200Ch;
	}
	public RadioButton getRbTop100Ch() {
		return rbTop100Ch;
	}
	public void setRbTop100Ch(RadioButton rbTop100Ch) {
		this.rbTop100Ch = rbTop100Ch;
	}
	public RadioButton getRbTop100Ww() {
		return rbTop100Ww;
	}
	public void setRbTop100Ww(RadioButton rbTop100Ww) {
		this.rbTop100Ww = rbTop100Ww;
	}
	public RadioButton getRbTopBibl() {
		return rbTopBibl;
	}
	public void setRbTopBibl(RadioButton rbTopBibl) {
		this.rbTopBibl = rbTopBibl;
	}
}