package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SpacerView {

	public HBox makeSpacer() {
		HBox spacerBox = new HBox(7, createSpacer());
		return spacerBox;
	}

	private Rectangle createSpacer() {
		Rectangle rect = new Rectangle(55, 55);
		rect.setFill(Color.TRANSPARENT);
		return rect;
	}
}
