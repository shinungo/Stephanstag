package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class LetterProposal {
	private TextField textProposal;
	public HBox createLetterProposal() {	
		Label labelProposal = new Label("Bitte Anfangsbuchstaben eingeben:");
		labelProposal.setId("labelProposalCss");
		textProposal = new TextField();
		textProposal.setMaxSize(160, 13);
		textProposal.setPromptText("Hier");
		textProposal.getText(); 
		HBox.setHgrow(textProposal, Priority.ALWAYS);
		HBox genderchoose1 = new HBox(7, labelProposal, textProposal);
		return genderchoose1;
	}
	public String getLetter() {
		return textProposal.getText();
	}
	public void clearLetterField() {
		textProposal.clear();
	}
}
