package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class AnimationSpaceHandler {

	private Timeline timeline;

	public GridPane constructAnimation() {

		timeline = new Timeline();
		Image image = new Image("ZHAW_LOGO.png");
		ImageView iv2 = new ImageView();
		iv2.setPreserveRatio(true);
		iv2.setFitWidth(70);
		iv2.setSmooth(true);
		iv2.setCache(true);
		iv2.setImage(image);

		Text babyNameText = new Text("BabyNamen");
		babyNameText.setFont(Font.font(56));

		StackPane topTextstack = new StackPane(babyNameText);
		StackPane logoStack = new StackPane(iv2);

		topTextstack.setAlignment(Pos.CENTER_LEFT);
		double sceneWidth = topTextstack.getWidth();
		double textWidth = babyNameText.getLayoutBounds().getWidth();
		Duration SecondPartDuration = Duration.seconds(6);
		Duration ArrivalDuration = Duration.seconds(9);
		Duration ActionDuration = Duration.seconds(9.05);
		Duration EndofActionDuration = Duration.seconds(9.25);
		Duration EndofActionSleep = Duration.seconds(9.38);
		Duration GoBackDuration = Duration.seconds(9.4);
		Duration AtendDuration = Duration.seconds(13);

		// Animation für sich bewegenden Text
		KeyValue startKeyValue = new KeyValue(babyNameText.translateXProperty(), sceneWidth);
		KeyFrame startKeyFrame = new KeyFrame(SecondPartDuration, startKeyValue);
		timeline.getKeyFrames().add(startKeyFrame);

		KeyValue endKeyValue = new KeyValue(babyNameText.translateXProperty(), 0.61 * textWidth);
		KeyFrame endKeyFrame = new KeyFrame(ArrivalDuration, endKeyValue);
		timeline.getKeyFrames().add(endKeyFrame);

		KeyValue start2KeyValue = new KeyValue(babyNameText.translateXProperty(), 0.61 * textWidth);
		KeyFrame start2KeyFrame = new KeyFrame(GoBackDuration, start2KeyValue);
		timeline.getKeyFrames().add(start2KeyFrame);

		KeyValue end2KeyValue = new KeyValue(babyNameText.translateXProperty(), sceneWidth);
		KeyFrame end2KeyFrame = new KeyFrame(AtendDuration, end2KeyValue);
		timeline.getKeyFrames().add(end2KeyFrame);

		// Animation für Logo
		KeyValue start3KeyValue = new KeyValue(logoStack.scaleXProperty(), 1);
		KeyValue keyValueY_LOGO = new KeyValue(logoStack.scaleYProperty(), 1);
		KeyFrame key3StartLogoActionFrame = new KeyFrame(ActionDuration, start3KeyValue, keyValueY_LOGO);
		timeline.getKeyFrames().add(key3StartLogoActionFrame);

		KeyValue start4KeyValue = new KeyValue(logoStack.scaleXProperty(), 1.4);
		KeyValue start4KeyVOGO = new KeyValue(logoStack.scaleYProperty(), 1.4);
		KeyFrame key4EndLogoActionFrame = new KeyFrame(EndofActionDuration, start4KeyValue, start4KeyVOGO);
		timeline.getKeyFrames().add(key4EndLogoActionFrame);

		EventHandler<ActionEvent> onFinished = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				System.out.println("jetst countet er im ActionHandler FX_Label Decoraton");
				timeline.setDelay(Duration.millis(00));
			}
		};

		KeyValue EndofActionSleepX = new KeyValue(logoStack.scaleXProperty(), 1);
		KeyValue EndofActionSleepY = new KeyValue(logoStack.scaleYProperty(), 1);
		KeyFrame EndofActionSleepFrame = new KeyFrame(EndofActionSleep, onFinished, EndofActionSleepX,
				EndofActionSleepY);
		
		timeline.getKeyFrames().add(EndofActionSleepFrame);
		timeline.setCycleCount(3);
		timeline.setDelay(Duration.millis(10000));
		timeline.play();

		GridPane grid = new GridPane();
		GridPane.setHgrow(topTextstack, Priority.ALWAYS);
		GridPane.setVgrow(topTextstack, Priority.ALWAYS);
		GridPane.setHgrow(logoStack, Priority.SOMETIMES);
		GridPane.setVgrow(logoStack, Priority.SOMETIMES);
		grid.add(topTextstack, 0, 0);
		grid.add(logoStack, 1, 0);
		return grid;

	}
}
