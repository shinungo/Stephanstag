package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

abstract class MenuAbstract {

	public String menuText;
	public EventHandler<ActionEvent> value;
	public KeyCode code;
	public KeyCombination modifiers;
	public Controller controller;
	public Menu menuFile;

	protected MenuItem menuExit = createMenuItem("_Beenden", event -> controller.exitDecision(), KeyCode.X,
			KeyCombination.CONTROL_DOWN);

	protected MenuItem menuLogin = createMenuItem("_LogIn", event -> controller.loginDecision(), KeyCode.L,
			KeyCombination.CONTROL_DOWN);

	protected MenuItem createMenuItem(String menuText, EventHandler<ActionEvent> value, KeyCode code,
			KeyCombination.Modifier... modifiers) {
		MenuItem item = new MenuItem(menuText);
		item.setAccelerator(new KeyCodeCombination(code, modifiers));
		item.setOnAction(value);
		return item;
	}
}
