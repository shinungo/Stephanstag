package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class GenderHandler {

	private Button buttonboy;
	private Button buttongirl;
	private String geschlecht = null;
	
	public VBox constructGenderBox() {
		Label genderchoostext = new Label("Bitte wählen Sie hier das Geschlecht:");
		genderchoostext.setId("genderchoostextcss");
		buttonboy = new Button("Junge");
		buttonboy.setId("buttonboyCss");
		buttonboy.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				buttonboy.setText("Junge Gewählt");
				buttongirl.setText("Mädchen nicht Gewählt");
				geschlecht = "m";
			}
			
		});
		buttongirl = new Button("Mädchen");
		buttongirl.setId("buttonGirlCss");
		buttongirl.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				buttongirl.setText("Mädchen Gewählt");
				buttonboy.setText("Junge nicht Gewählt");
				geschlecht = "w";

			}
		});
		HBox GenderChoiseBox = new HBox(7, createSpacer(), buttonboy, buttongirl, createSpacer());
		VBox GenderButton2 = new VBox(genderchoostext, GenderChoiseBox);
		return GenderButton2;
	}

	private Rectangle createSpacer() {
		Rectangle rect = new Rectangle(15, 20);
		rect.setFill(Color.TRANSPARENT);
		return rect;
	}
	
	public String getGeschlecht() {
		return geschlecht;
	}
	
	public void clearButtons() {
		buttonboy.setText("Junge");
		buttongirl.setText("Mädchen");
		geschlecht = null;
	}
}
