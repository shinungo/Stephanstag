package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import ch.shinungo.babyname.model.Babynamen;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class AddingHandler {

	private Controller controller;
	private Button addButton;
	private Button logOutButton;
	private GridPane grid;
	private HBox buttonGrid;
	private VBox vBox;
	private Label AddingName;
	private String a; 
	private String b; 
	private String weiblich = "w";
	private String maennlich = "m";
	private String bibl = "Keine";

	private TextField addName;
	private TextField addGeschlecht;
	private TextField addRanglisteSchweiz;
	private TextField addRanglisteAllerNamen;
	private TextField addRanglisteWelt;
	private TextField addBiblisch;

	public AddingHandler(Controller c) {
		this.controller = c;
	}

	public void gridForAddingNames() {
		grid = new GridPane();
		grid.setId("addNameCss");
		grid.setVgap(5);
		grid.setHgap(2);

		addName = new TextField();
		addName.setMaxWidth(132);
		addName.setPromptText("name");
		GridPane.setConstraints(addName, 0, 0);
		grid.getChildren().add(addName);

		addGeschlecht = new TextField();
		addGeschlecht.setMaxWidth(50);
		addGeschlecht.setPromptText("m/w");
		GridPane.setConstraints(addGeschlecht, 1, 0);
		grid.getChildren().addAll(addGeschlecht);

		addRanglisteSchweiz = new TextField();
		addRanglisteSchweiz.setMaxWidth(75);
		addRanglisteSchweiz.setPromptText("RangCH");
		GridPane.setConstraints(addRanglisteSchweiz, 2, 0);
		grid.getChildren().add(addRanglisteSchweiz);

		addRanglisteAllerNamen = new TextField();
		addRanglisteAllerNamen.setMaxWidth(82);
		addRanglisteAllerNamen.setPromptText("RangAll");
		GridPane.setConstraints(addRanglisteAllerNamen, 3, 0);
		grid.getChildren().add(addRanglisteAllerNamen);

		addRanglisteWelt = new TextField();
		addRanglisteWelt.setMaxWidth(82);
		addRanglisteWelt.setPromptText("RangWelt");
		GridPane.setConstraints(addRanglisteWelt, 4, 0);
		grid.getChildren().add(addRanglisteWelt);

		addBiblisch = new TextField();
		addBiblisch.setMaxWidth(155);
		addBiblisch.setPromptText("Biblisch");
		GridPane.setConstraints(addBiblisch, 5, 0);
		grid.getChildren().add(addBiblisch);
		
		AddingName = new Label("Erfassen Sie hier bitte einen Babynamen:");
		AddingName.setId("AddingNamecss");
		addButton = new Button("Hinzufügen");
		addButton.setId("addButtonCss");
	}

	public void checkNewName() {
		a = addName.getText().toString().trim();
		b = addGeschlecht.getText().toString().trim();
		String c = addRanglisteSchweiz.getText().toString().trim();
		String d = addRanglisteAllerNamen.getText().toString().trim();
		String e = addRanglisteWelt.getText().toString().trim();
		String f = addBiblisch.getText().toString().trim();


		if (a.length() == 0 || a.matches("\\d*")) {
			controller.displayAlert(Alert.AlertType.INFORMATION, "Bitte name eingeben!");

		} else if (!b.equals(weiblich) != b.equals(maennlich)) {
			controller.displayAlert(Alert.AlertType.INFORMATION, "Bitte m oder w eingeben");

		} else if (!c.matches("\\d*") || !d.matches("\\d*") || !e.matches("\\d*")) {
			controller.displayAlert(Alert.AlertType.INFORMATION, "Bei Ranglisten bitte Zahlen eingeben");

		} else if (!f.equals(bibl) && f.isEmpty()) {
			controller.displayAlert(Alert.AlertType.INFORMATION, "Bei biblisch bitte 'Keine' oder Text eingeben");
		
		
		} else {

			addNewName();
		}
	}

	private void addNewName() {
		Babynamen babynamenAdded = new Babynamen(addName.getText(), addGeschlecht.getText(),
				addRanglisteSchweiz.getText(), addRanglisteAllerNamen.getText(), addRanglisteWelt.getText(),
				addBiblisch.getText());
		controller.storeBabynamen(babynamenAdded);
		clearAddingFields();
	}

	private void clearAddingFields() {
		addName.clear();
		addGeschlecht.clear();
		addRanglisteAllerNamen.clear();
		addRanglisteSchweiz.clear();
		addRanglisteWelt.clear();
		addBiblisch.clear();
	}

	private Rectangle createSpacer() {
		Rectangle rect = new Rectangle(400, 10);
		rect.setFill(Color.TRANSPARENT);
		return rect;
	}

	public VBox createGridForAddingNames() {
		controller.getBabynamen();
		gridForAddingNames();
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				checkNewName();
			}
		});
		logOutButton = new Button("Verlassen");
		logOutButton.setId("logOutButtonCss");
		logOutButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				controller.removeViewsByLogOUt();
			}
		});
		buttonGrid = new HBox();
		buttonGrid.setId("vBoxAddingSectionCss");
		vBox = new VBox();
		buttonGrid.getChildren().addAll(addButton, createSpacer(), logOutButton);
		vBox.getChildren().addAll(AddingName, grid, buttonGrid);
		return vBox;
	}
}
