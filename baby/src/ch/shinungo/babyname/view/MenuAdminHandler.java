package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;

public class MenuAdminHandler extends MenuAbstract {
	private Controller controller;

	public MenuAdminHandler(Controller controller) {
		this.controller = controller;
	}

	public MenuItem menuExit = createMenuItem("_Beenden", event -> controller.exitDecision(), KeyCode.X,
			KeyCombination.CONTROL_DOWN);

	public MenuBar constructMenu() {
		MenuBar menuBar = new MenuBar();
		Menu menuFile = new Menu("_Programm");
		menuFile.setMnemonicParsing(true);
		MenuItem menuLogOut = createMenuItem("LogOut", event -> controller.removeViewsByLogOUt(), KeyCode.O,
				KeyCombination.CONTROL_DOWN);
		MenuItem menuOpen = createMenuItem("Liste Importieren",
				event -> controller.openFileChooser(Controller.FILE_OPEN), KeyCode.I, KeyCombination.CONTROL_DOWN);
		MenuItem menuSave = createMenuItem("Liste _speichern",
				event -> controller.openFileChooser(Controller.FILE_SAVE), KeyCode.S, KeyCombination.CONTROL_DOWN);

		menuFile.getItems().addAll(menuLogOut, menuOpen, menuSave, menuExit);
		menuBar.getMenus().addAll(menuFile);
		return menuBar;
	}
}