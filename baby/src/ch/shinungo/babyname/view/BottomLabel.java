package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import javafx.scene.control.Label;

public class BottomLabel {
	Label bottomText = new Label("Copywright by Markus Bolliger and Daniel da Silva");
	public Label makeAbottomText() {	
		bottomText.setId("bottomTextcss");
		return bottomText;
	}
}