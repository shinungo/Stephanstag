package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import ch.shinungo.babyname.model.Babynamen;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;

public class TabellenAnsicht {

	private Controller controller;

	public TabellenAnsicht(Controller controller) {
		this.controller = controller;
	}

	private TableView<Babynamen> table = new TableView<Babynamen>();
	VBox vBox = new VBox();
	TableColumn<Babynamen, String> columnF1 = createTableColumn("Name", Babynamen.NAME);
	TableColumn<Babynamen, String> columnF2 = createTableColumn("m/w", Babynamen.GESCHLECHT);
	TableColumn<Babynamen, String> columnF3 = createTableColumn("RangCH", Babynamen.RANGLISTESCHWEIZ);
	TableColumn<Babynamen, String> columnF4 = createTableColumn("RangAll", Babynamen.RANGLISTEALLERNAMEN);
	TableColumn<Babynamen, String> columnF5 = createTableColumn("RangWelt", Babynamen.RANGLISTEWELT);
	TableColumn<Babynamen, String> columnF6 = createTableColumn("Biblisch", Babynamen.BIBLISCH);

	@SuppressWarnings("unchecked")
	public VBox showTabellenAnsicht() {
		columnF1.setMinWidth(132);
		columnF2.setMinWidth(50);
		columnF3.setMinWidth(75);
		columnF4.setMinWidth(82);
		columnF5.setMinWidth(82);
		columnF6.setMinWidth(155);
		table.setItems(controller.getBabynamen());
		table.getColumns().addAll(columnF1, columnF2, columnF3, columnF4, columnF5, columnF6);
		vBox.setId("tableBoxcss");
		vBox.getChildren().addAll(table);
		return vBox;
	}

	private TableColumn<Babynamen, String> createTableColumn(String name, String p) {
		TableColumn<Babynamen, String> tc = new TableColumn<Babynamen, String>(name);
		tc.setCellValueFactory(new PropertyValueFactory<Babynamen, String>(p));

		tc.setCellFactory(TextFieldTableCell.forTableColumn());
		tc.setOnEditCommit(new EventHandler<CellEditEvent<Babynamen, String>>() {
			@Override
			public void handle(CellEditEvent<Babynamen, String> t) {
				((Babynamen) t.getTableView().getItems().get(t.getTablePosition().getRow())).setName(t.getNewValue());
			}
		});
		return tc;
	}
	public Babynamen getSelectedBaby() {
		return table.getSelectionModel().getSelectedItem();
	}
	public void closeWritableFunction() {
		table.setEditable(false);
	}
	public void openWritableFunction() {
		table.setEditable(true);
	}
}
