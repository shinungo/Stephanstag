package ch.shinungo.babyname.view;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import ch.shinungo.babyname.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class RemovingHandler {

	private Controller controller;
	private HBox removButtonBox;
	private Label removingName;
	private Button removeButton;

	public RemovingHandler(Controller d) {
		this.controller = d;
	}

	public HBox createButtonForDeletingNames() {
		removingName = new Label("Bitte markieren Sie auf den zu löschenden Namen");
		removingName.setId("RemoveSectionCss");
		removeButton = new Button("Löschen");
		removeButton.setId("removButtonCSS");

		removeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				controller.removeSelectedBabynamen();
			}
		});
		removButtonBox = new HBox(7, removingName, removeButton);
		return removButtonBox;
	}
}