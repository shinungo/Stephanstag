package ch.shinungo.babyname.model;

/**
 * Babynamen - Finale Version written by Markus Bolliger & Daniel da Sivla
 * ©-ZHAW-2019
 **/

public class Suchparameter {
	public Suchparameter() { }
    private String geschlecht = "";
	private String beginntMit = "";
    private boolean isrbTop200Ch = false;
    private boolean isrbTop100Ch = false;
    private boolean isrbTop100Ww = false;
    private boolean isrbTopBibl = false;

    public String getGeschlecht() {
        return geschlecht;
    }
	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
    }
	public String getBeginntMit() {
        return beginntMit;
    }
    public void setBeginntMit(String beginntMit) {
        this.beginntMit = beginntMit;
    }
	public boolean isTop200CH() {
		return isrbTop200Ch;
	}
	public void setTop200CH(boolean isTop200CH) {
		this.isrbTop200Ch = isTop200CH;
	}
	public boolean isrbTop100Ch() {
		return isrbTop100Ch;
	}
	public void setrbTop100Ch(boolean isrbTop100Ch) {
		this.isrbTop100Ch = isrbTop100Ch;
	}
	public boolean isrbTop100Ww() {
		return isrbTop100Ww;
	}
	public void setisrbTop100Ww(boolean isrbTop100Ww) {
		this.isrbTop100Ww = isrbTop100Ww;
	}
	public boolean isrbTopBibl() {
		return isrbTopBibl;
	}
	public void setisrbTopBibl(boolean isrbTopBibl) {
		this.isrbTopBibl = isrbTopBibl;
	}
}