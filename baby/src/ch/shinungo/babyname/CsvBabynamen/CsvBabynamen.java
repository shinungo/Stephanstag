package ch.shinungo.babyname.CsvBabynamen;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import ch.shinungo.babyname.model.Babynamen;
import ch.shinungo.babyname.model.Suchparameter;
import ch.shinungo.babyname.persistence.PersistenceBaby;


//Diese Klasse liest und schreibt das File und erledigt die Suchabfrage
public class CsvBabynamen implements PersistenceBaby{


	private static final String SPLITTER = ";";
	private List<Babynamen> names = new ArrayList<>();

	public List<Babynamen> getNames() {
		return names;
	}

	// Fügt einen neuen Eintrag in die Datei hinzu mit den Werten, welche in den Parametern übergeben werden
	public List<Babynamen> add(String name, String geschlecht, String ranglisteSchweiz, String ranglisteAllerNamen, String ranglisteWelt, String biblisch ) {
		Babynamen bn = new Babynamen(name, geschlecht, ranglisteSchweiz, ranglisteAllerNamen, ranglisteWelt, biblisch);
		names.add(bn);
		return names;	
	}

	public Babynamen csvBabynamen(String line, String splitter) {
		String [] result = line.split(";|\r|\\.");
		return new Babynamen(result[0], // Name
				result[1], // Geschlecht
				result[2],  // RanglisteSchweiz
				result[3],  // RanglisteAllerNamen
				result[4],  // RanglisteWelt
				result[5]); // Biblisch
	}

	// Kompletter Suchmechanismus 

	public List<Babynamen> search(Suchparameter params, String path) {
        
		//Sicherstellung, dass wenn kein Geschlecht ausgewählt wird
      if(params.getGeschlecht() == null) {
         params.setGeschlecht("");
      }
      
      List<Babynamen> all = getAll(path);
            
      //Filtersuche aufgrund der Auswahl im GUI
      return all
      	.stream()

				.filter(p -> p.getName().startsWith(params.getBeginntMit()) || params.getBeginntMit().equals(""))
          .filter(p-> p.getGeschlecht().equals(params.getGeschlecht()) || params.getGeschlecht().equals(""))
          .filter(p-> getInt(p.getRanglisteSchweiz()) > 0 && getInt(p.getRanglisteSchweiz()) <= 200 || !params.isTop200CH())
          .filter(p-> getInt(p.getRanglisteAllerNamen()) > 0 && getInt(p.getRanglisteAllerNamen()) <= 100 || !params.isrbTop100Ch())
          .filter(p-> getInt(p.getRanglisteWelt()) > 0 && getInt(p.getRanglisteWelt()) <= 100 || !params.isrbTop100Ww())
          .filter(p-> !p.getBiblisch().equals("Keine") || !params.isrbTopBibl())
          .collect(Collectors.toList());                
	}
	
  // Gibt den integer Wert des Strings zurück oder 0 wenn der string nicht konvertiert werden kann. 
	private int getInt(String value) {
      int retVal = 0;
      try {  
          retVal = Integer.parseInt(value);
      } catch (NumberFormatException e) {  
        retVal = 0;
      }
      return retVal;
   }

	@Override
	public List<Babynamen> getAll(String path) {
		List<Babynamen> babynames = new ArrayList<Babynamen>();		 
		BufferedReader br;		 
		try {
			br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String[] fields = line.split(SPLITTER, -1);

				Babynamen babynamen = new Babynamen(fields[0], fields[1], fields[2], fields[3], fields[4], fields[5]);
				babynames.add(babynamen);
			}
		} catch (FileNotFoundException ex) {
		} catch (IOException ex) {
		}
		return babynames;
	}


	@Override
	public void saveBabyname(Babynamen baby, String path) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
			out.println(serializeBabynameToData(baby));
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveAll(List<Babynamen> babies, String path) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
			Iterator<Babynamen> it = babies.iterator();
			while (it.hasNext()) {
				out.println(serializeBabynameToData(it.next()));
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String serializeBabynameToData(Babynamen babyname) {
		return babyname.getName() + SPLITTER + babyname.getGeschlecht() + SPLITTER + babyname.getRanglisteSchweiz()
		+ SPLITTER + babyname.getRanglisteAllerNamen() + SPLITTER + babyname.getRanglisteWelt() + SPLITTER
		+ babyname.getBiblisch();

	}

	@Override
	public void removeBabyname(Babynamen baby, String path1) {

		List<Babynamen> all = getAll(path1);
		Iterator<Babynamen> it = all.iterator();
		while (it.hasNext()) {
			Babynamen b = it.next();
			if (b.equals(baby)) {
				all.remove(baby);
				break;
			}
		}
		try {
			Files.delete(Paths.get(path1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		saveAll(all, path1);
	}
}