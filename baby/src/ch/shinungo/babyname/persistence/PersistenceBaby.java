package ch.shinungo.babyname.persistence;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/


// kann für einen DB-Anschluss verwendet werden, 

import java.util.List;

import ch.shinungo.babyname.model.Babynamen;
import ch.shinungo.babyname.model.Suchparameter;

public interface PersistenceBaby {

	public static final String DEFAULT_CSV_FILE = "src/BabyNamenKomplett.csv";
	public List<Babynamen> getAll(String path);
	public void saveBabyname(Babynamen baby, String path);
	public void removeBabyname(Babynamen baby, String path);
	public void saveAll(List<Babynamen> babies, String path);
	public List<Babynamen> search(Suchparameter params, String path);
}