package junit;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Sivla
 *  ©-ZHAW-2019
 **/

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;

import ch.shinungo.babyname.CsvBabynamen.CsvBabynamen;
import ch.shinungo.babyname.model.Babynamen;
import ch.shinungo.babyname.model.Suchparameter;

public class CsvBabynamenTest {
	
	//Testmethode für die Methode "getAll" (Lesen des Files in eine Liste)
	@Test
	public void testGetAll() {
		CsvBabynamen obj = new CsvBabynamen();
		List<Babynamen> babynamen = obj.getAll("src/BabyNamenKomplett.csv");
		
		//Testen ob Objekt null ist
		assertNotNull(babynamen);
		
		//Testen ob Liste Einträge hat
		assertFalse(babynamen.isEmpty());
		
		//Testen ob komplettes File geladen wurde (bzw. mindestanzahl -> durch hinzufügen im GUI können es auch mehr Zeilen werden)
		assertTrue(babynamen.size() > 59000);
	}
	
	@Test
	public void testSearch() {
		String path = "src/BabyNamenKomplett.csv";
		CsvBabynamen obj = new CsvBabynamen();
		
		//Leere Suche -> Alle Einträge sollten in der Liste sein
		Suchparameter params = new Suchparameter();
		
		assertFalse(obj.search(params, path) == null);
		
		// Suche nach männlichen Namen -> in etwa die Hälfte (zwischen 25'000 und 35'000
		// Einträge)
		params = new Suchparameter();
		params.setGeschlecht("m");
		assertTrue(obj.search(params, path).size() < 35000 && obj.search(params, path).size() > 25000);
		
		//Suche nach Top 100 Rangliste Schweiz
		params = new Suchparameter();
		params.setTop200CH(true);
		params.setGeschlecht("m");
		assertEquals(200, obj.search(params, path).size());
		
	}
	
	//Test Konvertierung eines Objektes in csv String
	@Test
	public void testSerializeBabynameToData() {
		Babynamen baby = new Babynamen("Daniel", "m", "50", "1", "1000", "Keine");
		CsvBabynamen obj = new CsvBabynamen();
		
		assertEquals("Daniel;m;50;1;1000;Keine", obj.serializeBabynameToData(baby));
		
	}

	//Test Konvertierung eines CSV Strings in ein Objekt
	@Test
	public void testCsvBabynamen() {
		CsvBabynamen obj = new CsvBabynamen();
		Babynamen baby = obj.csvBabynamen("Markus;m;120;0;1875;Keine", ";");
		
		assertEquals("Markus", baby.getName());
		assertEquals("m", baby.getGeschlecht());
		assertEquals("120", baby.getRanglisteSchweiz());
		assertEquals("0", baby.getRanglisteAllerNamen());
		assertEquals("1875", baby.getRanglisteWelt());
		assertEquals("Keine", baby.getBiblisch());
	}
}