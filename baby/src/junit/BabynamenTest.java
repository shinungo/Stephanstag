package junit;

/**
 *  Babynamen - Finale Version 
 *  written by Markus Bolliger & Daniel da Silva
 *  ©-ZHAW-2019
 **/

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import ch.shinungo.babyname.model.Babynamen;

public class BabynamenTest {
	
	@Test
	public void testConstructorAndGetter() {
		Babynamen baby = new Babynamen("Daniel", "m", "50", "1", "1000", "Keine");
		
		assertFalse(baby == null);
		
		assertEquals("Daniel", baby.getName());
		assertEquals("m", baby.getGeschlecht());
	}
	
	@Test
	public void testSetter() {
		Babynamen baby = new Babynamen("", "", "", "", "", "");
		System.out.println(baby.getName());
		baby.setName("Mikula");
		assertEquals("Mikula", baby.getName());
		
	}

}
